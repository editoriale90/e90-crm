<?php

function get_report() {
  global $wpdb;
  $query = "SELECT * FROM storico";
  $resultset = $wpdb -> get_results($wpdb -> prepare($query, OBJECT)) or die ('Errore nel recuperare i dati!');
  return $resultset;
}


function display_report() {
  ?>

  <!-- DataTables CSS library -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA==" crossorigin="anonymous"></script>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/dataTables.jqueryui.min.css"/>

  <!-- jQuery $ jQuery UI libraries -->
  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.min.js" integrity="sha256-lnH4vnCtlKU2LmD0ZW1dU7ohTTKrcKP50WA9fa350cE=" crossorigin="anonymous"></script>

  <!-- DataTables JS library -->
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/dataTables.jqueryui.min.js"></script>

  <!-- DataTables Buttons -->
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>

  <!-- DataTables Export table -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>


  <style>
  table td {
    font-size: 12px;
  }
  td {
    text-align: center;
    vertical-align: middle;
  }
  h1, h2 {
    text-align: center;
  }
  </style>


  <header>
    <br/>
    <h1>Storico abbonamenti Il Salvagente</h1>
    <br/><br/>

  <div>
    <table id="report" class="display">
      <thead>
        <tr>
          <th>ID</th>
          <th>Mese</th>
          <th>Benvenuto</th>
          <th>Benvenuto con guida</th>
          <th>Con guida</th>
          <th>Senza guida</th>
          <th>Manualpay in scadenza</th>
          <th>Totale</th>
        </tr>
      </thead>

      <tbody>
        <tr>
          <?php
          $resultset = get_report($wpdb);
          foreach ($resultset as $row) {  ?>
            <td><?php echo $row -> id; ?></td>
            <td><?php echo ita_month(beauty_date($row -> mese)); ?></td>
            <td><?php echo $row -> benvenuto; ?></td>
            <td><?php echo $row -> benvenuto_guida; ?></td>
            <td><?php echo $row -> guida; ?></td>
            <td><?php echo $row -> no_guida; ?></td>
            <td><?php echo $row -> manual; ?></td>
            <td><?php echo $row -> totale; ?></td>
          </tr>
        <?php } ?>
      </tbody>

    </table>
  </div>

  <script>
  $(document).ready(function() {
    var $dTable = $('#report').DataTable( {
    //  "order": [[ 0, "asc" ]],
      dom: 'Bfrtip',
      buttons: ['pageLength', 'csv', 'excel'],
    });
  });
  </script>

<?php
}
?>
