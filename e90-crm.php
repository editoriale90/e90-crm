<?php
/**
* Plugin Name: Ed90 CRM
* Description: Il CRM di Fiamma
* Author: Valentina per Editoriale Novanta
* Version: 0.3
*/

include 'subscriptions.php';
include 'shippings.php';
include 'orders.php';
include 'report.php';
include 'utilities.php';


  function crm_menu() {
    add_menu_page(
      'CRM',                   //page title
      'Abbonati',              //menu title
      'manage_options',        //capability
      'flame_crm',             //menu slug
      'display_subscriptions', //callable function
      'dashicons-visibility',  //icon
      6                        //position
    );
    add_submenu_page(
      'flame_crm',            //parent slug
      'spedizioni',           //page title
      'Spedizioni',           //menu title
      'manage_options',       //capability
      'shippings',            //menu slug
      'display_shippings'     //function
    );
    add_submenu_page(
      'flame_crm',            //parent slug
      'ordini',               //page title
      'Ordini',               //menu title
      'manage_options',       //capability
      'orders',               //menu slug
      'display_orders'        //function
    );
    add_submenu_page(
      'flame_crm',            //parent slug
      'storico',              //page title
      'Storico',              //menu title
      'manage_options',       //capability
      'report',               //menu slug
      'display_report'        //function
    );
  }
  add_action('admin_menu', 'crm_menu');


  ?>
