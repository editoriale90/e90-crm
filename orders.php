<?php

function get_orders() {
  global $wpdb;
  $query = " SELECT * FROM ordini ";
  $resultset = $wpdb -> get_results($wpdb -> prepare($query, OBJECT)) or die ('Errore nel recuperare i dati!');
  return $resultset;
}



function display_orders() {
  ?>


  <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

  <link href="https://nightly.datatables.net/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
  <script src="https://nightly.datatables.net/js/jquery.dataTables.js"></script>

  <!-- Select2 plugin -->
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

  <!-- DataTables Export table -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>

  <!-- DataTables Buttons -->
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>

  <link href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />


  <meta charset=utf-8 />

  <style>
    .ui-datepicker td .ui-state-default {
      padding: .2em!important;
    }
    #baseDateControl {
      float:left;
    }
  </style>


  <header>
    <br/>
    <h1>Lista ordini - Salvagente</h1>
    <br/>
    <h2>Ultimo aggiornamento: <?php print_r(ita_month(show_uptime())); ?></h2>
    <br/>
  </header>



  <table id="ordini" class="display compact" style="width:100%">

    <thead>
      <tr>
        <th>N. Ord.</th>
        <th>Cognome</th>
        <th>Nome</th>
        <th>Prodotto</th>
        <th>Data</th>
        <th>Stato</th>
        <th>E-mail</th>
        <th>Telefono</th>
        <th>Indirizzo</th>
        <th>Citt�</th>
        <th>Nazione</th>
        <th>Totale</th>
        <th>Metodo pagamento</th>
      </tr>
    </thead>


        <tfoot>
          <tr>
            <th>N. Ord.</th>
            <th>Cognome</th>
            <th>Nome</th>
            <th>Prodotto</th>
            <th>Data</th>
            <th>Stato</th>
            <th>E-mail</th>
            <th>Telefono</th>
            <th>Indirizzo</th>
            <th>Citt�</th>
            <th>Nazione</th>
            <th>Totale</th>
            <th>Metodo pagamento</th>
          </tr>
        </tfoot>

      <tbody>
        <tr>
          <?php
          $resultset = get_orders();
          foreach ($resultset as $row) {  ?>
            <td><a href="<?php get_site_url() ?>/wp-admin/post.php?post=<?php echo $row -> id_ordine; ?>&action=edit" target="_blank"><?php echo $row -> id_ordine; ?></a></td>
            <td><?php echo $row -> cognome; ?></td>
            <td><?php echo $row -> nome; ?></td>
            <td><?php echo $row -> item; ?></td>
            <td><?php echo beauty_date($row -> data_ord); ?></td>
            <td><?php echo beautystr($row -> stato); ?></td>
            <td><?php echo $row -> email; ?></td>
            <td><?php echo $row -> tel; ?></td>
            <td><?php echo $row -> societ� .' '. $row -> indirizzo1 .' '. $row -> indirizzo2 ; ?></td>
            <td><?php echo $row -> cap . ' '. $row -> citta .' '. $row -> prov; ?></td>
            <td><?php echo $row -> nazione; ?></td>
            <td><?php echo 'E. ', $row -> tot; ?></td>
            <td><?php
              if (($row -> pagamento) == null || ($row -> pagamento) == '') {
                echo 'manual pay';
              }
              else {
                echo $row -> pagamento;
              } ?></td>
          </tr>
        <?php } ?>
      </tbody>

    </table>


    <script>

    $(document).ready( function () {

      var table = $('#ordini').DataTable({

                "order": [[ 4, "desc" ]],

                dom: 'Bfrtip',
                buttons: [
                  'pageLength', 'csv', {
                  extend: 'pdfHtml5',
                  orientation: 'landscape',
                  pageSize: 'LEGAL'
                  },
                  {
                    extend: 'excelHtml5',
                    text: 'Excel'
                  }
                ], // buttons

                initComplete: function () {

                count = 0;
                this.api().columns([3,5,12]).every( function () {
                    var title = this.header();
                    //replace spaces with dashes
                    title = $(title).html().replace(/[\W]/g, '-');
                    var column = this;
                    var select = $('<select id="' + title + '" class="select2" ></select>')
                         .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                          //Get the "text" property from each selected data
                          //regex escape the value and store in array
                          var data = $.map( $(this).select2('data'), function( value, key ) {
                            return value.text ? '^' + $.fn.dataTable.util.escapeRegex(value.text) + '$' : null;
                                     });

                          //if no data selected use ""
                          if (data.length === 0) {
                            data = [""];
                          }

                          //join array into string with regex or (|)
                          var val = data.join('|');

                          //search for the option(s) selected
                          column
                                .search( val ? val : '', true, false )
                                .draw();
                        } );

                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' );
                    } );

                  //use column title as selector and placeholder
                  $('#' + title).select2({
                    multiple: true,
                    closeOnSelect: false,
                    placeholder: "" + title
                  });

                  //initially clear select otherwise first option is selected
                  $('.select2').val(null).trigger('change');
                } );

            } // initComplete

      }); //dataTable

    } ); // document.ready


    </script>

<?php
}
?>
