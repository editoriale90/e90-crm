<?php

function beautystr($stringa) {

  // piano
  if ($stringa == 'year') {
    $stringa = 'annuale';
    return $stringa;
  }
  if ($stringa == 'month') {
    $stringa = 'mensile';
    return $stringa;
  }

  // stato
  if ($stringa == 'wc-expired') {
    $stringa = 'scaduto';
    return $stringa;
  }
  if ($stringa == 'wc-on-hold') {
    $stringa = 'in attesa';
    return $stringa;
  }
  if ($stringa == 'wc-cancelled') {
    $stringa = 'cancellato';
    return $stringa;
  }
  if ($stringa == 'wc-active') {
    $stringa = 'attivo';
    return $stringa;
  }
  if ($stringa == 'wc-pending') {
    $stringa = 'sospeso';
    return $stringa;
  }
  if ($stringa == 'wc-pending-cancel') {
    $stringa = 'in attesa di cancellazione';
    return $stringa;
  }
  if ($stringa == 'wc-completed') {
    $stringa = 'completato';
    return $stringa;
  }
  if ($stringa == 'wc-refunded') {
    $stringa = 'rimborsato';
    return $stringa;
  }
  if ($stringa == 'wc-failed') {
    $stringa = 'fallito';
    return $stringa;
  }
  if ($stringa == 'wc-wcf-main-order') {
    $stringa = 'main order';
    return $stringa;
  }
  if ($stringa == 'wc-processing') {
    $stringa = 'in lavorazione';
    return $stringa;
  }
  else {
    return $stringa;
  }

}


function beauty_date($data) {
  if ($data != 0) {
    return date('d M Y', strtotime($data));
  }
  else
  return '-';
}
function beauty_datetime($data) {
  if ($data != 0) {
    return date('d M Y, G:i', strtotime($data));
  }
  else
  return '-';
}

function ita_month($date) {
  $eng = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
  $ita = array("Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug", "Ago", "Set", "Ott", "Nov", "Dic");
  return $ita_date = str_replace($eng, $ita, $date);
}


function show_uptime() {
  global $wpdb;
  $query = "SELECT CONVERT_TZ(create_time,'+00:00','+01:00') as ora FROM INFORMATION_SCHEMA.TABLES WHERE table_name = 'utenti'";
  $resultset = $wpdb -> get_results($wpdb -> prepare($query, OBJECT)) or die ('Errore nel recuperare dei dati!');
  $var = $resultset[0] -> ora;
  $datetime = beauty_datetime($var);
  return $datetime;
}


function simpl_item($pre_str) {
  $post_str = substr($pre_str, strpos($pre_str, " ") + 1);
  $final = substr($post_str, strpos($post_str, '"'));
  if (strpos($pre_str, 'BASIC') !== false) {
    $final = 'Informato';
  }
  elseif (strpos($pre_str, 'PREMIUM') !== false) {
    $final = 'Esperto';
  }
  switch ($final) {
    case '"CONSUMATORE INFORMATO"':
    $final = 'Informato';
    break;
    case '"CONSUMATORE ESPERTO"';
    $final = 'Esperto';
    break;
  }
  return $final;
}

function if_abb($pre_str) {
  $taglia = substr($pre_str, 0, 11);
  if ($taglia == "Abbonamento") {
    return $stringa = substr($pre_str, 12);
  }
  else return $pre_str;
}

function cambio_status($row) {
  if ($row != 0) {
    $diff = time() - strtotime($row);
    $gg = round($diff / (60 * 60 * 24));
    return intval($gg);
  }
  else return " - ";
}


?>
