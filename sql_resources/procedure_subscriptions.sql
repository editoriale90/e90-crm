DROP EVENT IF EXISTS cthulhu;

DELIMITER //

CREATE EVENT cthulhu
ON SCHEDULE EVERY 8 HOUR
#STARTS '2020-11-09 14:00:00'
STARTS NOW()
DO
	BEGIN
    
		DECLARE n INT DEFAULT 0;
		DECLARE id_abb INT DEFAULT 0;
		DECLARE i INT DEFAULT 0;
		DECLARE nome VARCHAR(100);
		DECLARE cognome VARCHAR(100);
		DECLARE totale LONGTEXT;
		DECLARE prox_pag LONGTEXT;
		DECLARE inizio_abb LONGTEXT;
		DECLARE fine_abb LONGTEXT;
		DECLARE email LONGTEXT;
		DECLARE piano LONGTEXT;
        DECLARE tel LONGTEXT;
        DECLARE pagamento LONGTEXT;
        DECLARE id_prodotto INT DEFAULT 0;
		
		DROP TABLE utenti;
		CREATE TABLE utenti (
			codice INT,
			cognome VARCHAR(100),
			nome VARCHAR(100),
			totale LONGTEXT,
			inizio_abb LONGTEXT,
			prox_pag LONGTEXT,
			fine_abb LONGTEXT,
			email LONGTEXT,
			piano LONGTEXT,
            tel LONGTEXT,
            pagamento LONGTEXT,
            id_prodotto INT
            );
		
		SELECT COUNT(*) FROM wp_posts WHERE wp_posts.post_type = 'shop_subscription' INTO n;
		
		SET i = 0;
		WHILE i < n DO 
			 SELECT id FROM wp_posts where post_type = 'shop_subscription' LIMIT i, 1 INTO id_abb;
			 SELECT meta_value FROM wp_postmeta WHERE meta_key = '_billing_last_name' AND post_id = id_abb INTO cognome;
			 SELECT meta_value FROM wp_postmeta WHERE meta_key = '_billing_first_name' AND post_id = id_abb INTO nome;
			 SELECT meta_value FROM wp_postmeta WHERE meta_key = '_order_total' AND post_id = id_abb INTO totale;
			 SELECT meta_value FROM wp_postmeta WHERE meta_key = '_schedule_next_payment' AND post_id = id_abb INTO prox_pag;
			 SELECT meta_value FROM wp_postmeta WHERE meta_key = '_schedule_start' AND post_id = id_abb INTO inizio_abb;
			 SELECT meta_value FROM wp_postmeta WHERE meta_key = '_schedule_end' AND post_id = id_abb INTO fine_abb;
			 SELECT meta_value FROM wp_postmeta WHERE meta_key = '_billing_email' AND post_id = id_abb INTO email;
			 SELECT meta_value FROM wp_postmeta WHERE meta_key = '_billing_period' AND post_id = id_abb INTO piano;
             SELECT meta_value FROM wp_postmeta WHERE meta_key = '_billing_phone' AND post_id = id_abb INTO tel;
             
             SET pagamento = '';
			 SELECT meta_value FROM wp_postmeta WHERE meta_key = '_payment_method' AND meta_value IS NOT NULL AND post_id = id_abb INTO pagamento;
             
			 SELECT meta_value FROM wp_woocommerce_order_items it
				JOIN wp_woocommerce_order_itemmeta m ON it.order_item_id = m.order_item_id
				WHERE m.meta_key = '_product_id' AND it.order_id = id_abb
				LIMIT 1 INTO id_prodotto;
             
			 INSERT INTO utenti VALUES(id_abb, cognome, nome, totale, inizio_abb, prox_pag, fine_abb, email, piano, tel, pagamento, id_prodotto);
			 SET i = i + 1;             
		END WHILE;
    
	END //

DELIMITER ;