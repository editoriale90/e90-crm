DROP EVENT IF EXISTS ordini;
drop table if exists ordini;

DELIMITER //

CREATE EVENT ordini
ON SCHEDULE AT CURRENT_TIMESTAMP

DO
	BEGIN

		DECLARE n INT;
		DECLARE i INT;
		DECLARE ordine INT;
		DECLARE cognome VARCHAR(100);
		DECLARE nome VARCHAR(100);
        DECLARE soc VARCHAR(100);
		DECLARE email VARCHAR(100);
        DECLARE tel VARCHAR(100);
        DECLARE indirizzo1 VARCHAR(100);
        DECLARE indirizzo2 VARCHAR(100);
        DECLARE citta VARCHAR(100);
        DECLARE cap VARCHAR(100);
        DECLARE prov VARCHAR(100);
        DECLARE nazione VARCHAR(100);
        DECLARE tot VARCHAR(100);
        DECLARE pagamento VARCHAR(100);


        CREATE TABLE ordini (
			id INT auto_increment PRIMARY KEY,
			id_ordine INT,
            item VARCHAR(100),
			cognome VARCHAR(100),
			nome VARCHAR(100),
            soc VARCHAR(100),
			email VARCHAR(100),
			tel VARCHAR(100),
			data_ord DATETIME,
            stato VARCHAR(100),
			indirizzo1 VARCHAR(100),
			indirizzo2 VARCHAR(100),
			citta VARCHAR(100),
			cap VARCHAR(100),
			prov VARCHAR(100),
            nazione VARCHAR(100),
			tot VARCHAR(100),
			pagamento VARCHAR(100)
        );


		INSERT INTO ordini(id_ordine, item, data_ord, stato)
            SELECT id, order_item_name, post_date_gmt, post_status FROM wp_posts
				JOIN wp_woocommerce_order_items it ON id = order_id
                jOIN wp_woocommerce_order_itemmeta m ON it.order_item_id = m.order_item_id
				WHERE post_type = 'shop_order' AND order_item_type = 'line_item'
                AND m.meta_key = '_product_id'
                AND meta_value NOT IN(
					11224, 14435, 15913, 44503, 44579, 44571, 44750, 44798, 44815, 44829,
                    44578, 44830, 66681, 66683, 65882, 72570, 76976, 85826, 83320, 100665,
                    101020, 111312, 56585
                )
				AND order_item_name NOT LIKE 'Abbonamento%';



        SELECT COUNT(*) FROM ordini LIMIT 1 INTO n;

        SET i = 1;
        WHILE i <= n DO

			SELECT id_ordine FROM ordini WHERE id = i LIMIT 1 INTO ordine;

			# COGNOME
			SELECT meta_value FROM wp_postmeta WHERE meta_key = '_shipping_last_name' AND post_id = ordine LIMIT 1 INTO cognome;
			SELECT meta_value FROM wp_postmeta WHERE meta_key = '_billing_last_name'
				AND (SELECT meta_value FROM wp_postmeta WHERE meta_key = '_shipping_last_name' AND post_id = ordine) = ''
				AND post_id = ordine LIMIT 1 INTO cognome;

			# NOME
			SELECT meta_value FROM wp_postmeta WHERE meta_key = '_shipping_first_name' AND post_id = ordine LIMIT 1 INTO nome;
			SELECT meta_value FROM wp_postmeta WHERE meta_key = '_billing_first_name'
				AND (SELECT meta_value FROM wp_postmeta WHERE meta_key = '_shipping_first_name' AND post_id = ordine) = ''
				AND post_id = ordine LIMIT 1 INTO nome;

            # SOCIETA'
			SELECT meta_value FROM wp_postmeta WHERE meta_key = '_shipping_company' AND post_id = ordine LIMIT 1 INTO soc;
			SELECT meta_value FROM wp_postmeta WHERE meta_key = '_billing_company'
				AND (SELECT meta_value FROM wp_postmeta WHERE meta_key = '_shipping_company' AND post_id = ordine) = ''
				AND post_id = ordine LIMIT 1 INTO soc;

			# EMAIL
			SELECT meta_value FROM wp_postmeta WHERE meta_key = '_billing_email' AND post_id = ordine LIMIT 1 INTO email;

			# TELEFONO
			SELECT meta_value FROM wp_postmeta WHERE meta_key = '_billing_phone' AND post_id = ordine LIMIT 1 INTO tel;

			# INDIRIZZO1
			SELECT meta_value FROM wp_postmeta WHERE meta_key = '_shipping_address_1' AND post_id = ordine LIMIT 1 INTO indirizzo1;
			SELECT meta_value FROM wp_postmeta WHERE meta_key = '_billing_address_1'
				AND (SELECT meta_value FROM wp_postmeta WHERE meta_key = '_shipping_address_1' AND post_id = ordine) = ''
				AND post_id = ordine LIMIT 1 INTO indirizzo1;

			# INDIRIZZO2
            SET indirizzo2 = "";
			SELECT meta_value FROM wp_postmeta WHERE meta_key = '_shipping_address_2' AND post_id = ordine LIMIT 1 INTO indirizzo2;
			SELECT meta_value FROM wp_postmeta WHERE meta_key = '_billing_address_2'
				AND (SELECT meta_value FROM wp_postmeta WHERE meta_key = '_shipping_address_1' AND post_id = ordine) = ''
				AND post_id = ordine LIMIT 1 INTO indirizzo2;

			# CITTA
			SELECT meta_value FROM wp_postmeta WHERE meta_key = '_shipping_city' AND post_id = ordine LIMIT 1 INTO citta;
			SELECT meta_value FROM wp_postmeta WHERE meta_key = '_billing_city'
				AND (SELECT meta_value FROM wp_postmeta WHERE meta_key = '_shipping_city' AND post_id = ordine) = ''
				AND post_id = ordine LIMIT 1 INTO citta;

            # CAP
			SELECT meta_value FROM wp_postmeta WHERE meta_key = '_shipping_postcode' AND post_id = ordine LIMIT 1 INTO cap;
			SELECT meta_value FROM wp_postmeta WHERE meta_key = '_billing_postcode'
				AND (SELECT meta_value FROM wp_postmeta WHERE meta_key = '_shipping_postcode' AND post_id = ordine) = ''
				AND post_id = ordine LIMIT 1 INTO cap;

			# PROVINCIA
			SELECT meta_value FROM wp_postmeta WHERE meta_key = '_shipping_state' AND post_id = ordine LIMIT 1 INTO prov;
			SELECT meta_value FROM wp_postmeta WHERE meta_key = '_billing_state'
				AND (SELECT meta_value FROM wp_postmeta WHERE meta_key = '_shipping_state' AND post_id = ordine) = ''
				AND post_id = ordine LIMIT 1 INTO prov;

			# NAZIONE
			SELECT meta_value FROM wp_postmeta WHERE meta_key = '_shipping_country' AND post_id = ordine LIMIT 1 INTO nazione;
			SELECT meta_value FROM wp_postmeta WHERE meta_key = '_billing_country'
				AND (SELECT meta_value FROM wp_postmeta WHERE meta_key = '_shipping_country' AND post_id = ordine) = ''
				AND post_id = ordine LIMIT 1 INTO nazione;

			# TOTALE
			SELECT meta_value FROM wp_postmeta WHERE meta_key = '_order_total' AND post_id = ordine LIMIT 1 INTO tot;

			# PAGAMENTO
			SELECT meta_value FROM wp_postmeta WHERE meta_key = '_payment_method' AND post_id = ordine LIMIT 1 INTO pagamento;

            UPDATE ordini
				SET cognome = cognome,
					nome = nome,
                    soc = soc,
					email = email,
					tel = tel,
					indirizzo1 = indirizzo1,
					indirizzo2 = indirizzo2,
					citta = citta,
					cap = cap,
					prov = prov,
                    nazione = nazione,
					tot = tot,
					pagamento = pagamento
				WHERE id = i;

			SET i = i + 1;
		END WHILE;

	END //

DELIMITER ;

		
